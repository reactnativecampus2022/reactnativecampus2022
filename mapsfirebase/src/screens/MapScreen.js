import React from 'react'; 
import {View, Text, Image } from "react-native"; 

import MapView, {PROVIDER_GOOGLE ,Circle , Marker, Callout} from 'react-native-maps'; 

import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import Geolocation from 'react-native-geolocation-service';


import { PermissionsAndroid } from 'react-native';


const MapScreen = (props) => {
    const {route, navigation} = props

    const initialLocation = {latitude:  37.78825, longitude: -122.4324} 

    const [location, setLocation] = React.useState(initialLocation)


    const requestLocationPermission = async () => {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: "Cool Photo App Camera Permission",
              message:
                "Cool Photo App needs access to your camera " +
                "so you can take awesome pictures.",
              buttonNeutral: "Ask Me Later",
              buttonNegative: "Cancel",
              buttonPositive: "OK"
            }
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera");
          } else {
            console.log("Camera permission denied");
          }
        } catch (err) {
          console.warn(err);
        }
      };

    React.useEffect( () => {

        requestLocationPermission(); 

        console.log("location: ", location)




        Geolocation.getCurrentPosition(
            (position) => {
              console.log(position);

              const location =  {latitude:  position.coords.latitude, longitude: position.coords.longitude} 

              setLocation(location)

            },
            (error) => {
              // See error code charts below.
              console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    },[])
    
    return(
        <View style={{flex: 1 }}>
            {/* <Text  style={{textAlign: "center", color: "#000", fontSize: 14}}>Map screen</Text> */}
            
            <GooglePlacesAutocomplete
                placeholder='Search'
                fetchDetails
                GooglePlacesSearchQuery={{
					rankby: "distance"
				}}
                onPress={(data, details = null) => {
                    // 'details' is provided when fetchDetails = true
                    console.log(data, details);
                }}
                onFail={res => console.log("res: ", res) }
                onTimeout={() => console.log("hello")}
                
                query={{
                    key: 'AIzaSyBgQLQ0lzIAe8pBet6nPeqV6IZQjSSa1K0',
                    language: 'en',
                    // components: "country:us",
					// types: "establishment",
					// radius: 30000,
                    // location: " 37.78825, -122.4324"
                        
                }}

                styles={{
                    container: { flex: 0, position: "absolute", width: "90%", borderRadius: 8, zIndex: 1, margin: 16 },
                    listView: { backgroundColor: "white", position: "absolute", zIndex: 1 }
                }}
            />
            
            <MapView
                style={{flex: 1,
                    position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,}}
                provider={PROVIDER_GOOGLE}
                zoomEnabled
                initialRegion={{
                    latitude: 37.4219524,
                    longitude: -122.0840976,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}>


                    <Marker
                        //key={0}
                        coordinate={{
                            latitude: location?.latitude,
                                longitude: location?.longitude,}}
                        title='Test Title'
                        description='TEST DESCRIPTION'
                        >
                            <Callout tooltip
                                        onPress={() => console.log("CALLOUT PRESSES")}
                            >
                                <View style={{ backgroundColor: '#fcd000', padding: 16}}>
                                    
                                    
                                    <Text style={{fontSize: 14, color: "#000", fontWeight: "bold"}}>
                                        Welcome to my awesome company    
                                    </Text>

                                    <Text style={{fontSize: 12, color: "#000"}}>
                                        Press the marker for more info    
                                    </Text>       
                                </View> 
                            </Callout>
                        </Marker>

                        <Circle
                                center= {{
                                    latitude: 37.78825,
                                longitude: -122.4324,
                                }}

                                radius={1000}
                            />
                </MapView>
        </View>
    )
}

export default MapScreen; 