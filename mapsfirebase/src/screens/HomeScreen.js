import React, {useEffect} from 'react';
import {View, Text, TouchableOpacity, Alert} from 'react-native'; 
import messaging from '@react-native-firebase/messaging';


const HomeScreen = (props) => {

    
    useEffect(() => {

        // messaging().subscribeToTopic('news')
        //             .then(() => console.log('Subscribed to topic!'));

        messaging().getToken().then(
            res => console.log("token: ",  res)
        )

        const unsubscribe = messaging().onMessage(async remoteMessage => {
          Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
        });
    
        return unsubscribe;
      }, []);




    return (
        <View style={{padding: 16 }}>
            <Text>Hello HomeScreen</Text>


            <TouchableOpacity onPress={() => props.navigation.navigate("Map")}>
                <View style={{padding: 16, marginVertical: 8, borderWidth: 1, borderRadius: 8}}>
                    <Text style={{color: "#000", fontSize: 14}}>
                        Google Maps Advanced
                    </Text>
                </View>
            </TouchableOpacity>





            <TouchableOpacity onPress={() =>  props.navigation.navigate("Second")}>
                <View style={{padding: 16, marginVertical: 8, borderWidth: 1, borderRadius: 8}}>
                    <Text style={{color: "#000", fontSize: 14}}>
                        Firebase Auth
                    </Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() =>  props.navigation.navigate("Webview")}>
                <View style={{padding: 16, marginVertical: 8, borderWidth: 1, borderRadius: 8}}>
                    <Text style={{color: "#000", fontSize: 14}}>
                        webview example
                    </Text>
                </View>
            </TouchableOpacity>


        </View>
    )
}

export default HomeScreen