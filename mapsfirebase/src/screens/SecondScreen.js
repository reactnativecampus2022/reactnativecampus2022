import React, {useState, useEffect} from 'react'; 
import {View, Text, TouchableOpacity} from 'react-native'

import auth from '@react-native-firebase/auth';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';


const MyButton = (props) => {
    return (
        <TouchableOpacity {...props}>
        <View style={{padding: 16, marginVertical: 8, borderWidth: 1, borderRadius: 8}}>
            <Text style={{color: "#000", fontSize: 14}}>
                {props.title}
            </Text>
        </View>
    </TouchableOpacity>
    )
}



const SignUpUser = (email, passswod) => {
    return new Promise(function(resolve, reject) {
      auth()
        .createUserWithEmailAndPassword(email, passswod)
        .then(() => {
          resolve('Sign Up Successfully');
        })
        .catch(error => {
          reject(error);
        });
    });
  };
  
const SignInUser = (email, passswod) => {
    return new Promise(function(resolve, reject) {
      auth()
        .signInWithEmailAndPassword(email, passswod)
        .then(res => {
          resolve('Sign In Successfully');
          console.log("")
          console.log("@email/password sign in ")
          console.log("")

          console.log("res: ", res)


        })
        .catch(error => {
            console.log("ERROR: ", error)
          reject(error);
        });
    });
  };
  
const SignOutUser = () => {
    return new Promise(function(resolve, reject) {
      auth()
        .signOut()
        .then(()=> {
            console.log("")
            console.log("SIGN OUT successffuly")
            console.log("")
  
            
          resolve('Sign Out Successfully');
        })
        .catch(error => {
          reject(error);
        });
    });
  };

const SecondScreen = (props) => {

    // // Set an initializing state whilst Firebase connects
    // const [initializing, setInitializing] = useState(true);
    // const [user, setUser] = useState();

    // // Handle user state changes
    // function onAuthStateChanged(user) {
    //     setUser(user);
    //     console.log("@user:")
    //     if (initializing) setInitializing(false);
    // }


        
    async function onGoogleButtonPress() {
        // Get the users ID token
        const { idToken } = await GoogleSignin.signIn();
    
        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    
        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    }

    useEffect(() => {
        //const subscriber = auth().onAuthStateChanged(onAuthStateChanged);

        //console.log("Subscriber: ", subscriber); 


        GoogleSignin.configure({
            webClientId: '654899995527-kg4agi1pj4bqhiiimlnmvkmo0rmiaf02.apps.googleusercontent.com',
          });
        //return subscriber; // unsubscribe on unmount
    }, []);


    return (
        <View style={{padding: 16}}>
            <Text style={{color: "#000", fontSize: 14}}>
                Firebase Screen
            </Text>


            <MyButton title="SignUp" onPress={() => SignUpUser("reactnative@google.com", "helloworld")}/>
            <MyButton title="Login" onPress={() => SignInUser("anas@gmail.com", "1jfjjf78")}/>
            <MyButton title="SignOut" onPress={SignOutUser }/>

            
            <MyButton title="Google sign in "  onPress={() => onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}  /> 


        </View>
    )
}

export default SecondScreen