import React, { useEffect} from 'react'; 
import {View, Text} from "react-native"; 

import { WebView } from 'react-native-webview';

const WebviewScreen = (props) => {

    const htmlParagraph = `<h1>Hello world</h1> <p style="font-size:34pt">Text size using points.</p><p style="font-size:18px">Text size using pixels.</p><p style="font-size:larger">Text size using relative sizes.</p>`


    return(
        <View style={{flex: 1, padding: 16, backgroundColor: "#FFF"}}>

            <View  style={{flex: 1, padding: 16, backgroundColor: "#FFF"}}>
                    <WebView source={{ uri: 'https://reactnative.dev/' }} />
            </View>

            <View  style={{flex: 1, padding: 16, backgroundColor: "#FFF"}}>
            <WebView
                    originWhitelist={['*']}
                    source={{ html: htmlParagraph }}
                />
            </View>


            <Text>
                {htmlParagraph}
            </Text>
  
        </View>
    )
}

export default WebviewScreen; 