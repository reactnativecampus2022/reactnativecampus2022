import React from 'react';  

import { createStackNavigator  } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

//Screens
import HomeScreen from './screens/HomeScreen';
import MapScreen from './screens/MapScreen';
import SecondScreen from './screens/SecondScreen';
import WebviewScreen from './screens/WebviewScreen';
// import DetailsScreen from './screens/DetailsScreen';
// import EditScreen from './screens/EditScreen';
// import MapScreen from './screens/MapScreen';

const Stack = createStackNavigator(); 

const AppNavigator = () => {
    return(
        <NavigationContainer>

            <Stack.Navigator>
                
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Map" component={MapScreen} />
                <Stack.Screen name="Second" component={SecondScreen} />
                <Stack.Screen name="Webview" component={WebviewScreen} />
            
            </Stack.Navigator>

        </NavigationContainer>
    )
}

export default AppNavigator; 