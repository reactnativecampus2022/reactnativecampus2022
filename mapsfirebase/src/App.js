import React from 'react'
import AppNavigator from './AppNavigator';



import messaging from '@react-native-firebase/messaging';



// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
  });

const App = () => {
    return (
        <AppNavigator />
    )
}

export default App; 