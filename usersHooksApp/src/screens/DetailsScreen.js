import React, { useState, useEffect } from 'react';
import {View, Text, TouchableOpacity } from 'react-native'; 

import data from '../MOCK_DATA'; 
/**
 * 
 * Mock user 
 */


import MyButtonComponent from '../components/MyButtonComponent';

//  const MyButton = (props) => {
//     return(
//         <TouchableOpacity {...props}>
//                 <View style={{  padding: 16, 
//                                 backgroundColor: props.colorButton,
//                                 borderWidth: 1, 
//                                 borderRadius: 6,
//                                 justifyContent: "center", 
//                                 alignItems: "center"}}>

//                     <Text style={{  color: "#000",
//                                     fontSize: 14, 
                                    
//                                     }}>
//                         {props.title}
//                     </Text>
//                 </View>
//         </TouchableOpacity>
//     )
// } 


const DetailsScreen = (props) =>{
    const [user, setUser] = useState(null); 

    useEffect( () => {
       console.log("DetailsScreen: ", props); 
       setUser(data[1])
       console.log("user: ", user); 

       if(props.route.params?.user) {
           setUser(props.route.params.user)
       }
           
    }, [])

    return (
        <View style={{flex: 1, padding: 16 }}>
            
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold"}}>
                  Welcome to Details Screen
            </Text>
            <View  style={{flex: 1, padding: 16, borderWidth: 1}} >

                <Text  style={{color: "#000", fontSize:12 }} >
                   USER INFO
                </Text>
                {user  ? <Text  style={{color: "#000", fontSize:12 }} >
                    { JSON.stringify(user) }
                </Text>: null }

            </View>

            <View >
                <View style={{flexDirection: "row", marginVertical: 8, justifyContent: "space-around"}}>
                    <MyButtonComponent onPress= {() => console.log("Call Me")}
                                backgroundColor= "#00F"
                                title="Call" />
                    
                    <MyButtonComponent onPress= {() => console.log("Send Email")}
                                backgroundColor= "#F0F"
                                title="Email" />
                
                </View>
                <View style={{flexDirection: "row",  justifyContent: "space-around"}}>
                        <MyButtonComponent onPress= {() => props.navigation.navigate("Edit", {user} )}
                                        backgroundColor= "#00F"
                                        title="Edit" />
                            
                        <MyButtonComponent onPress= {() => props.navigation.navigate("Home", {deleteUserId: props.route.params?.user?.id })}
                                        backgroundColor= "#F0F"
                                        title="Delete" />
                </View>
            
            </View>
        </View>
    )
}

export default DetailsScreen; 