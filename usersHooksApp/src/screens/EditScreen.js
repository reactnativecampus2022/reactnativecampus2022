import React, { useState, useEffect } from 'react';
import {View, Text, ScrollView } from 'react-native'; 

//Components
import MyButtonComponent from '../components/MyButtonComponent';
import MyInputComponent from '../components/MyInputComponent';


const EditScreen = (props) =>{
    //const [user, setUser] = useState(null); 

    useEffect( () => {
       console.log("EditScreen: ", props); 
       const user = props.route.params?.user 

        if (user) {
            setFirstName(user?.firstName);
            setLastName(user?.lastName);
            setEmail(user?.email);
            setPhone(user?.phone);
            setCompany(user?.company)
        }

    }, [])

    //Form states
    const [firstName, setFirstName] = useState(""); 
    const [lastName, setLastName] = useState(""); 
    const [email, setEmail] = useState(""); 
    const [phone, setPhone] = useState(""); 
    const [company, setCompany] = useState(""); 

    return (
        <View style={{flex: 1, padding: 16 }}>
            
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold"}}>
                  Welcome to Edit Screen
            </Text>

           
            <ScrollView>
            <View style={{flex: 1 }}>

                <View style={{flex: 1 }}>

                    <MyInputComponent
                        onFocus={() => console.log("focused firstName")}
                        onChangeText={ text => setFirstName(text)}
                        value={firstName}
                    />
                     <MyInputComponent
                        onFocus={() => console.log("focused lastName")}
                        onChangeText={ text => setLastName(text)}
                        value={lastName}
                    />
                     <MyInputComponent
                        onFocus={() => console.log("focused Email")}
                        onChangeText={ text => setEmail(text)}
                        keyboardType="email-address"
                        value={email}
                    />
                     <MyInputComponent
                        onFocus={() => console.log("focused Phone")}
                        onChangeText={ text => setPhone(text)}
                        keyboardType="phone-pad"
                        value={phone}
                    />
                     <MyInputComponent
                        onFocus={() => console.log("focused company")}
                        onChangeText={ text => setCompany(text)}
                        value={company}
                    />
                    
                </View>

                <View>
                    <View style={{flex: 1, flexDirection: "row", marginVertical: 8, justifyContent: "space-around" }}>
                        
                        <MyButtonComponent onPress= {() =>{  

                                                    var modifiedAt = (new Date()).toDateString()
                                                    var newUserId = (new Date()).getTime() 

                                                    var user = {
                                                        id: newUserId, 
                                                        firstName, 
                                                        lastName, 
                                                        email, 
                                                        phone, 
                                                        company, 
                                                        createdAt: modifiedAt, 
                                                        modifiedAt
                                                    }

                                                    if( props.route.params?.user?.id ) {
                                                        user = {
                                                            ...user, 
                                                            
                                                            id: props.route.params.user.id,
                                                            createdAt: props.route.params.user.createdAt, 
                                                        }
                                                    }

                                                    console.log("@navigate: ", user)
                            
                                                    props.navigation.navigate("Home", {user: user } )
                                                
                                            }}
                                    colorButton= "#8DF574"
                                    title= { props.route.params?.user?.id ? "Edit User" : "Add User"} />

                        <MyButtonComponent onPress= {() => props.navigation.goBack()}
                                    colorButton= "#8DF574"
                                    title="Cancel" />
                    </View>
                </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default EditScreen; 