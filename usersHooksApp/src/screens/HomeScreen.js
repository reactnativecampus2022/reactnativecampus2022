import React, { useState, useEffect } from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native'; 

//Local DATA
import data from '../MOCK_DATA'

/**
 * 
 * User Model Object
 * 
 * user:  {
 *          id: 0, 
 *          firstName: "anas", 
 *          lastName: "hmamouch", 
 *          phone: "01234567", 
 *          company: "Capgemini", 
 *          createdAt: "jj/mm/yyyy", 
 *          modifiedAt: "jj/mm/yyyy"
 *    }
 */

 const FloatingButtonComponent = (props) => {
    return (

        <View
            style={{
                padding: 16,
                borderRadius: 36,
                backgroundColor: '#fcd000',
                position: 'absolute',
                right: 10,
                bottom: 10
            }}>

            <TouchableOpacity {...props}>
                <Text style={{ fontSize: 16, fontWeight: "bold", color:"black" }}>+ ADD USER</Text>    
            </TouchableOpacity>
        </View>

    )
}

const HomeScreen = (props) =>{
    const [users, setUsers] = useState([]); 

    useEffect( () => {
        if( data ){
            setUsers(data); 
        }
    }, [])

    useEffect( () => {
                        console.log("updated");

                        if(props.route.params?.user) {
                            console.log("@user: ", props.route.params?.user)
                            
                            var id = props.route.params?.user?.id 
                            var usersCopy = [...users]
                
                            usersCopy = usersCopy.filter(element => element.id !== id )

                            console.log(" @id: ", id); 
                            

                            usersCopy = [...usersCopy, props.route.params?.user]
                
                            ///setUsers(prevState => console.log(prevState)); 
                            
                            setUsers(usersCopy)
                        }

                    }, [props.route.params?.user] )

    useEffect( () => {
                    console.log("came from delete screen");

                    if(props.route.params?.deleteUserId) {
                        
                        var id = props.route.params?.deleteUserId 
                        var usersCopy = [...users]
            
                        usersCopy = usersCopy.filter(element => element.id !== id )

                        setUsers(usersCopy)
                    }

                }, [props.route.params?.deleteUserId] )


    return (
        <View style={{flex: 1, padding: 16 }}>
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold" }}>
                  Welcome to Users App
            </Text>

            {users ? <Text style={{ color: "#000", fontSize: 12 }}>
                        {"You have " + users.length + " users."}
                    </Text> : null}

            <FlatList 
                data={ users.reverse() }
                renderItem={({item, index}) => {
                    return(

                        //props.navigation.navigate()

                        <TouchableOpacity 
                                            onPress = {() => props.navigation.navigate("Details", {user: item}) }>
                            <View style={{padding: 8, marginVertical: 8,  borderWidth: 1, borderRadius: 4 }}>
                                
                                <Text style={{color: "#000"}}>
                                    {"Name: " +  item.firstName + item.lastName}
                                </Text>
                                
                                <Text style={{color: "#000"}}>
                                    {"Email: " +  item.email}
                                </Text>
                                {item?.phone ? 
                                                <Text style={{color: "#000"}}>
                                                    {"Phone: " +  item.phone}
                                                </Text> : null
                                }
                                
                                <Text style={{color: "#000"}}>
                                    {"Company: " +  item.company}
                                </Text>
                            </View>

                        </TouchableOpacity>
                    )
                }}
            />
            
            <FloatingButtonComponent onPress={() => props.navigation.navigate("Edit") }/>
        </View>
    )
}

export default HomeScreen; 