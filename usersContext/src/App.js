import React, {useContext, useState, createContext} from 'react'; 
import {View, Text, TouchableOpacity} from 'react-native'; 


const AppContext = createContext({
    userName: '',
    setUsername: () => {},
  })


const MyComponent = (props) => {
    const value = useContext(AppContext)
    return (
            <View style={{ borderWidth: 1, padding: 16 }}>
                 <Text  style={{ color: "#000", fontSize: 14, marginBottom: 8}}> 
                    {props.title}
                </Text>
                {props.showButton && <MyButton title="press me" onPress={ () => value.setUsername(props?.title)} /> }
                
                {props.children}
            </View>)
}

const MyButton = (props) => {
    return (
        <TouchableOpacity {...props}>
            <View style={{ borderWidth: 1, padding: 8, borderRadius: 8, backgroundColor: "#FCD000" }}>
                <Text style={{ color: "#000", fontSize: 14}}>
                    {props.title}
                </Text>
            </View>
        </TouchableOpacity>
        )

}


const App = () => {

    const [username, setUsername] = useState("")
    const value = {
        username,
        setUsername
    }

    return(
        <AppContext.Provider value={value}>
            <View style={{margin: 16}}>
                <MyComponent title="Parent Component #1" >
                    <Text style={{color: "#000", fontWeight:"bold"}}> My name is: {value.username} </Text>

                    <MyComponent title="Child Component #11" showButton >
                        <Text> My name is:  </Text>

                        <MyComponent title="Child Component #111" showButton>
                            <Text> My name is:  </Text>
                           {/* { <MyButton title="press me" onPress={() => console.log("Child Component #111")}/>} */}
                        </MyComponent>

                        <MyComponent title="Child Component #112" >
                            <Text> My name is:  </Text>
                            {/* <MyButton title="press me" onPress={() => console.log("Child Component #112")}/> */}
                        </MyComponent>

                    </MyComponent>

                </MyComponent>

                <View style={{margin: 8}}>

                <MyComponent title="Parent Component #2" >
                    <Text> My name is:  </Text>
                </MyComponent>

                <MyComponent  title="Parent Component #3" >
                    <Text> My name is:  </Text>

                    <MyComponent title="Child Component #31" showButton>
                        <Text> My name is:  </Text>
                    </MyComponent>

                </MyComponent>

                </View>

            </View>
        
        </AppContext.Provider>
    )
}


export default App; 