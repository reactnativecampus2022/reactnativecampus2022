/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../src/App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

import { render, fireEvent } from '@testing-library/react-native';



it('renders correctly', () => {
  renderer.create(<App />);
});

it('Should create an item', () => {
  const { getByText, getByPlaceholderText } = render(<App />)

  const addItemButton = getByText('Add'); 

  const textInput = getByPlaceholderText("write a text here");
  
  const itemCreated= "first item"

  fireEvent.changeText(textInput, itemCreated); 
  fireEvent.press(addItemButton);
  
  const item = getByText(itemCreated); 

  expect(item).not.toBeNull()
  expect(item).toBeTruthy()

  //check with jest
  
});
