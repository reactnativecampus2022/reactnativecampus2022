import React from 'react'
import {View, Text, TextInput, FlatList, TouchableOpacity} from 'react-native'

const MyButton = (props) => {
    return (
        <TouchableOpacity {...props} >
            <View style={{ backgroundColor: "#F0F", padding: 16 }}>
                <Text style={{ color: "#000", fontSize: 14 }} >
                    {props.title}
                </Text>
            </View>
        </TouchableOpacity>
    )

}

const App = () => {

    const [item, setItem] = React.useState("")
    const [listItem, setListItem] = React.useState([])

    const addItem = () => setListItem([...listItem, { id: Math.random()*1000, value:item} ]) 

    return (
        <View style={{flex: 1, backgroundColor: "#FFF", padding: 16}}>

            <View style={{ backgroundColor: "#FFF", padding: 16 }}>
                <Text style={{ color: "#000", fontSize: 14 }} >
                    Testing with Jest react-native-testing library
                </Text>

                <Text style={{ color: "#000", fontSize: 14 }} >
                    Check file ./__tests__/App-test.js
                </Text>
            </View>

            <View style={{flexDirection: "row"}}>

                <TextInput 
                    style={{flex: 1}}
                    accessibilityLabel='textInputLabel'
                    placeholder='write a text here'
                    underlineColorAndroid="#000"
                    onChangeText={text => setItem(text) }
                    value={item}
                />

                <TouchableOpacity 
                    accessibilityLabel='addButton'
                    onPress={addItem} >
                    <View style={{ backgroundColor: "#00F", paddingHorizontal: 32, borderRadius: 8, paddingVertical: 8, marginLeft: 8 }}>
                        <Text style={{ color: "#FFF", fontSize: 14 }} >
                            Add 
                        </Text>
                    </View>
                </TouchableOpacity>

            </View>

            <FlatList
            testID='flatlist'
                data={listItem}
                keyExtractor={item => String(item.id)}
                renderItem={({item}) => <View><Text style={{color: "#000", fontSize: 14}}  >{item.value}</Text></View>}
            />

            {/* {listItem.map(element => <View><Text style={{color: "#000", fontSize: 14}}  >{element.value}</Text></View>)}
           */}
           

        </View>
    )
}

export default App