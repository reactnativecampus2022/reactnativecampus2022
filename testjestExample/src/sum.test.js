import sum from './sum'
test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);

    // expect(sum(1, 42)).toBe(44);
    // expect(sum(1, 100)).toBe(103);
  });

  test('adds 1 + 54 to equal 45', () => {

    expect(sum(1, 59 )).toBe(61);
  });

  test('adds 1 + 102 to equal 105', () => {

    expect(sum(1, 102)).toBe(105);
  });