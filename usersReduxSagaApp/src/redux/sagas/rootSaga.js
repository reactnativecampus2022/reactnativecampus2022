import { 
    getAllUsersSagaWatcher,
    addUserSagaWatcher,
    updateUserSagaWatcher,
    deleteUserSagaWatcher
} from '../sagas/userSagas'; 

import { all } from 'redux-saga/effects'; 

export default function* rootSaga() {
    yield all([
        getAllUsersSagaWatcher(),
        addUserSagaWatcher(),
        updateUserSagaWatcher(),
        deleteUserSagaWatcher()
    ])
} 


