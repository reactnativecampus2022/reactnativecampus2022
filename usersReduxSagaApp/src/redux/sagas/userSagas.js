import { takeLatest, takeEvery, call, put, all, fork } from 'redux-saga/effects'; 

import * as actionTypes from "../actions/actionTypes"; 
import * as api from '../../services/api'; 

//Workers
function* getAllUsersSagaWorker (action) {
    try {
        const response = yield call( api.getUsers )
        yield put({ type: actionTypes.GET_USERS_SUCCESS, users: response.data })

    }catch(error) {
        console.log("error getAllUsersSagaWorker: ", error)
        yield put({ type: actionTypes.GET_USERS_FAILURE, error: error })
    }
}

function* addUserSagaWorker (action) {
    try {
        const response = yield call( api.addUser, action.user )
        
        yield put({ type: actionTypes.ADD_USER_SUCCESS, user: response.data })

    }catch(error) {
        console.log("error addUserSagaWorker: ", error)

        yield put({ type: actionTypes.ADD_USER_FAILURE, error: error })
    }
}

function* updateUserSagaWorker (action) {

    try {
        const response = yield call( api.modifyUserById, action.user?.id, action.user  )
        yield put({ type: actionTypes.UPDATE_USER_SUCCESS, user: response.data })

    }catch(error) {
        console.log("error updateUserSagaWorker: ", error)
        yield put({ type: actionTypes.UPDATE_USER_FAILURE, error: error })
    }
}

function* deleteUserSagaWorker (action) {
    //console.log("user to delete action ", action)
    try {
        const response = yield call( api.deleteUserById, action.id )
        yield put({ type: actionTypes.DELETE_USER_SUCCESS, id: action.id })

    }catch(error) {
        console.log("error deleteUserSagaWorker: ", error)
        yield put({ type: actionTypes.DELETE_USER_FAILURE, error: error })
    }
}

//Watchers
export function * getAllUsersSagaWatcher() {
    yield takeEvery(actionTypes.GET_USERS_REQUEST, getAllUsersSagaWorker)
}

export function * addUserSagaWatcher() {
    yield takeLatest(actionTypes.ADD_USER_REQUEST, addUserSagaWorker)
}

export function * updateUserSagaWatcher() {
    yield takeLatest(actionTypes.UPDATE_USER_REQUEST, updateUserSagaWorker)
}

export function * deleteUserSagaWatcher() {
    yield takeLatest(actionTypes.DELETE_USER_REQUEST, deleteUserSagaWorker)
}