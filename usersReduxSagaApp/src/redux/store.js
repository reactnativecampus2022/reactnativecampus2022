import {createStore, applyMiddleware} from "redux";

import createSagaMiddleware from "redux-saga"
 
import rootReducer from "./reducers/rootReducer";

import rootSaga from "./sagas/rootSaga";


const sagaMiddleware = createSagaMiddleware()

//my global store
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

//run rootSaga
sagaMiddleware.run(rootSaga)

export default store