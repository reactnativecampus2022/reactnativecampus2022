import React, {useEffect} from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native'; 

//components
import FloatingButtonComponent  from "../components/FloatingButtonComponent"; 

//redux
import { useSelector, useDispatch } from 'react-redux';

//actions to dispatch
import { getUser, getUserAction } from '../redux/actions/userActions';

import { getAllUsers } from '../redux/actions/userActions';

import * as API from '../services/api'; 

const HomeScreen = (props) =>{

    const users = useSelector(state => state.userReducer.users); 
    const dispatch = useDispatch();

    const navigateTo = (screenName, params ) => {

                                    var params = params ? params : {}

                                    dispatch(getUserAction(params) )
                                    props.navigation.navigate(screenName) 
                                
                                }

    useEffect(() => { 
            dispatch(getAllUsers()) 
    }, [])

    return (
        <View style={{flex: 1, padding: 16 }}>
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold" }}>
                  Welcome to Users App
            </Text>

            <FlatList 
                contentContainerStyle={{ paddingBottom: 24}}
                data={ users }
                ListEmptyComponent={() =>  
                                        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>

                                            <Text style={{ color: "#000", fontSize: 14, textAlign: "center"}}>No users available</Text>
                                            <Text style={{ color: "#000", fontSize: 14, textAlign: "center"}}>Please start adding users by clicking on ADD USER below.</Text>

                                        </View>}

                renderItem={({item, index}) => {

                    var {firstName, lastName, email, phone, company} = item 

                    var firstName = firstName ? firstName: "" 
                    var lastName = lastName ? lastName: "" 
                    var email = email ? email: "" 
                    var phone = phone ? phone: "" 
                    var company = company ? company: "" 
                    
                    
                    return(

                        <TouchableOpacity onPress = {() => navigateTo("Details", item) }>
                            
                            <View style={{padding: 8, marginVertical: 8,  borderWidth: 1, borderRadius: 4 }}>
                                
                                <Text style={{color: "#000"}}>
                                    {"Name: " +  firstName + " " + lastName}
                                </Text>
                                
                                <Text style={{color: "#000"}}>
                                    {"Email: " +  email}
                                </Text>

                                <Text style={{color: "#000"}}>
                                    {"Phone: " +   phone}
                                </Text> 
                                
                                <Text style={{color: "#000"}}>
                                    {"Company: " +  company}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />

            <FloatingButtonComponent onPress={() => navigateTo("Edit") }/>
        </View>
    )
}

export default HomeScreen; 