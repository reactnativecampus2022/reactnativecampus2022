export const GET_USERS="GET_USERS"; 
export const GET_USERS_SUCCESS="GET_USERS_SUCCESS"; 
export const GET_USERS_FAILURE="GET_USERS_FAILURE"; 

export const SET_USER="SET_USER"; 
export const ADD_USER="ADD_USER"; 
export const UPDATE_USER="UPDATE_USER"; 
export const DELETE_USER="DELETE_USER"; 