import * as actions from "./actionTypes"

import axios from "axios"; 

export const getAllUsersAction = (users) => ({type: actions.GET_USERS_SUCCESS, users})

export const getAllUsersErrorAction = (error) => ({type: actions.GET_USERS_FAILURE, error})

export const getUserAction = (user) => ({type: actions.SET_USER, user})

export const addUserAction = (user) => ({type: actions.ADD_USER, user})

export const updateUserAction = (user) => ({type: actions.UPDATE_USER, user})

export const deleteUserAction = (id) => ({type:actions.DELETE_USER, id })


export const getAllUsers = () => 
    async (dispatch, getState) => {

                console.log("@getState: ", getState())

                try{
                    const response = await axios.get("http://10.0.2.2:3000/users/")

                    console.log("@reponse: ", response.data)

                    dispatch( getAllUsersAction(response.data) )

                }catch(e) {
                    console.log("@error: ", e)
                    dispatch( getAllUsersErrorAction(e) )
                }
                
}

export const getUser = (user) => async (dispatch, getState) => {
        
    console.log("getState: ", getState()); 
    console.log("user: ", user)

    try{
        const response = await axios.get("http://10.0.2.2:3000/users/" + user?.id)

        console.log("@reponse: ", response.data)

        dispatch( getUserAction(response.data) )

    }catch(e) {
        console.log("@error: ", e)
    }

}

export const addUser = (user) => async (dispatch, getState) => {
        
    console.log("getState: ", getState()); 
    console.log("user: ", user)

    try{
        const response = await axios.post("http://10.0.2.2:3000/users/", user)

        console.log("post data response ", response.data)

        dispatch( addUserAction(response.data) )

    }catch(e) {
        console.log("@error: ", e)
    }

}

export const updateUser = (user) => async (dispatch, getState) => {
        
    console.log("getState: ", getState()); 
    console.log("userCopy: ", user )

    try{
        const response = await axios.put("http://10.0.2.2:3000/users/" + user?.id, user)

        console.log("put data response ", response.data)

        dispatch( updateUserAction(response.data) )

    }catch(e) {
        console.log("@error: ", e)
    }

}

export const deleteUser = (id) => async (dispatch, getState) => {
        
    console.log("getState: ", getState()); 
    console.log("id to delete: ", id )

    try{
        const response = await axios.delete("http://10.0.2.2:3000/users/" + id)

        console.log("delete data response ", response.data)

        dispatch( deleteUserAction(id) )

    }catch(e) {
        console.log("@error: ", e)
    }

}
