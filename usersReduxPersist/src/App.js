import React from 'react'; 

import AppNavigator from "./AppNavigator";

import { Provider } from 'react-redux'; 

import { PersistGate } from 'redux-persist/integration/react'

import { store, persistor} from './redux/store'; 


const App = () => {

    console.log("persistor: ", persistor.getState())

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <AppNavigator />
            </PersistGate>
        </Provider>
    )
}

export default App; 