import React, { useState, useEffect } from 'react';
import {View, Text,  } from 'react-native'; 

//Components 
import MyButtonComponent from '../components/MyButtonComponent';

//actions to dispatch
import { deleteUser } from '../redux/actions/userActions'; 

//redux
import { useSelector, useDispatch } from 'react-redux';

const DetailsScreen = (props) =>{

    const user = useSelector(state => state.userReducer.user)
    const dispatch = useDispatch()

    return (
        <View style={{flex: 1, padding: 16 }}>
            
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold"}}>
                  Welcome to Details Screen
            </Text>
            <View style={{ flex: 1, padding: 16, borderWidth: 1 }} >

                <Text style={{ color: "#000", fontSize: 12, marginBottom: 16 }} >
                    USER INFORMATION
                </Text>

                <Text style={{ color: "#000", fontSize: 12, marginBottom: 16 }} >
                    ID: {"#" + user?.id}
                </Text>


                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Name: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.firstName + " " + user?.lastName}
                    </Text>
                </Text>

                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Email: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.email}
                    </Text>
                </Text>

                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Phone: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.phone}
                    </Text>
                </Text>

                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Company: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.company}
                    </Text>
                </Text>

                <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "center" }}>

                    <Text style={{ color: "#000", fontSize: 16 }}>
                        Member since {user?.createdAt}
                    </Text>

                </View>

            </View>

            <View >
                <View style={{flexDirection: "row", marginVertical: 8, justifyContent: "space-around"}}>
                    {/* <MyButtonComponent onPress= {() => console.log("Call Me")}
                                backgroundColor= "#00F"
                                title="Call" />
                    
                    <MyButtonComponent onPress= {() => console.log("Send Email")}
                                backgroundColor= "#F0F"
                                title="Email" /> */}
                
                </View>
                <View style={{flexDirection: "row",  justifyContent: "space-around"}}>
                        <MyButtonComponent onPress= {() => props.navigation.navigate("Edit")}
                                        backgroundColor= "#00F"
                                        title="Edit" />

                        <MyButtonComponent onPress= {() =>{ dispatch(deleteUser(user?.id))
                                                            props.navigation.navigate("Home") 
                                                        }}
                                            backgroundColor= "#F0F"
                                            title="Delete" />
       
                </View>            
            </View>
        </View>
    )
}

export default DetailsScreen; 