import React from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native'; 

//components
import FloatingButtonComponent  from "../components/FloatingButtonComponent"; 

//redux
import { useSelector, useDispatch } from 'react-redux';

//actions to dispatch
import { setUser } from '../redux/actions/userActions';

const HomeScreen = (props) =>{

    const users = useSelector(state => state.userReducer.users); 
    const dispatch = useDispatch();

    const navigateTo = (screenName, params) => {
                                    dispatch(setUser(params))
                                    props.navigation.navigate(screenName) }

    return (
        <View style={{flex: 1, padding: 16 }}>
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold" }}>
                  Welcome to Users App
            </Text>

            <FlatList 
                contentContainerStyle={{flex: 1, paddingBottom: 24}}
                data={ users }
                ListEmptyComponent={() =>  
                                        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>

                                            <Text style={{ color: "#000", fontSize: 14, textAlign: "center"}}>No users available</Text>
                                            <Text style={{ color: "#000", fontSize: 14, textAlign: "center"}}>Please start adding users by clicking on ADD USER below.</Text>

                                        </View>}

                renderItem={({item, index}) => {
                    return(

                        <TouchableOpacity onPress = {() => navigateTo("Details", item)}>
                            
                            <View style={{padding: 8, marginVertical: 8,  borderWidth: 1, borderRadius: 4 }}>
                                
                                <Text style={{color: "#000"}}>
                                    {"Name: " +  item.firstName + " " +  item.lastName}
                                </Text>
                                
                                <Text style={{color: "#000"}}>
                                    {"Email: " +  item.email}
                                </Text>
                                {item?.phone ? 
                                                <Text style={{color: "#000"}}>
                                                    {"Phone: " +  item.phone}
                                                </Text> : null
                                }
                                
                                <Text style={{color: "#000"}}>
                                    {"Company: " +  item.company}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />

            <FloatingButtonComponent onPress={() => navigateTo("Edit", {})}/>
        </View>
    )
}

export default HomeScreen; 