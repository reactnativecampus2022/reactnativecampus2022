import React, { useState, useEffect } from 'react';
import {View, Text, ScrollView } from 'react-native'; 

//Components
import MyButtonComponent from '../components/MyButtonComponent';
import MyInputComponent from '../components/MyInputComponent';

//actions to dispatch
import { addUser, updateUser } from '../redux/actions/userActions';

//redux
import { useSelector, useDispatch } from 'react-redux';

const EditScreen = (props) =>{

    const user = useSelector(state  => state.userReducer.user )
    const dispatch = useDispatch()

    useEffect( () => {
       console.log("EditScreen ");
       
       console.log("")
       console.log(user)
       console.log("")
    
        if (user) {
            setFirstName(user?.firstName);
            setLastName(user?.lastName);
            setEmail(user?.email);
            setPhone(user?.phone);
            setCompany(user?.company)
        }

    }, [])

    //Form states
    const [firstName, setFirstName] = useState(""); 
    const [lastName, setLastName] = useState(""); 
    const [email, setEmail] = useState(""); 
    const [phone, setPhone] = useState(""); 
    const [company, setCompany] = useState(""); 

    return (
        <View style={{flex: 1, padding: 16 }}>
            
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold"}}>
                  Welcome to Edit Screen
            </Text>

            <ScrollView>
                <View style={{flex: 1 }}>

                    <View style={{flex: 1 }}>
                        <Text>First name</Text>
                            <MyInputComponent
                                onFocus={() => console.log("focused firstName")}
                                onChangeText={ text => setFirstName(text)}
                                value={firstName}
                            />

                        <Text>Last name</Text>
                            <MyInputComponent
                                onFocus={() => console.log("focused lastName")}
                                onChangeText={ text => setLastName(text)}
                                value={lastName}
                            />

                        <Text>Email</Text>
                            <MyInputComponent
                                onFocus={() => console.log("focused Email")}
                                onChangeText={ text => setEmail(text)}
                                keyboardType="email-address"
                                value={email}
                            />

                        <Text>Phone</Text>
                            <MyInputComponent
                                onFocus={() => console.log("focused Phone")}
                                onChangeText={ text => setPhone(text)}
                                keyboardType="phone-pad"
                                value={phone}
                            />
                        <Text>Company</Text>
                            <MyInputComponent
                                onFocus={() => console.log("focused company")}
                                onChangeText={ text => setCompany(text)}
                                value={company}
                            />
                    </View>

                    <View style={{flex: 1, flexDirection: "row", marginVertical: 8, justifyContent: "space-around" }}>
                            
                        <MyButtonComponent onPress= {() =>{  

                                                        var modifiedAt = (new Date()).toDateString()
                                                        var newUserId = (new Date()).getTime() 

                                                        var userCopy = {
                                                            id: newUserId, 
                                                            firstName, 
                                                            lastName, 
                                                            email, 
                                                            phone, 
                                                            company, 
                                                            createdAt: modifiedAt, 
                                                            modifiedAt
                                                        }

                                                        if( user?.id ) {
                                                            userCopy = {
                                                                ...userCopy, 
                                                                
                                                                id: user?.id,
                                                                createdAt: user?.createdAt, 
                                                            }

                                                            dispatch(updateUser(userCopy))

                                                        }else {
                                                            dispatch(addUser(userCopy))
                                                        }

                                                        console.log("user To dispatch: ", userCopy)
                                
                                                        props.navigation.navigate("Home");                                                 
                                                }}
                                            colorButton= "#8DF574"
                                            title= {  user?.id ? "Edit User" : "Add User"} />

                        <MyButtonComponent onPress= {() => props.navigation.goBack()}
                                                colorButton= "#8DF574"
                                                title="Cancel" />
                        </View>
                    </View>
            </ScrollView>
        </View>
    )
}

export default EditScreen; 