import React, {useEffect} from 'react';
import {View, Text, FlatList, TouchableOpacity, Switch} from 'react-native'; 

//components
import FloatingButtonComponent  from "../components/FloatingButtonComponent"; 

//redux
import { useSelector, useDispatch } from 'react-redux';

//actions to dispatch
//import { setUser } from '../redux/actions/userActions';


import { userSlice, fetchAllUsersThunk } from '../redux/slices/userSlice';

import {switchThunk, setUser, addUser, updateUser, deleteUser} from '../redux/slices/userSlice'; 


import MyButtonComponent from '../components/MyButtonComponent';

const HomeScreen = (props) =>{

    const storeToolkit = useSelector( state => state  ); 
    
    const users =  useSelector( state => state.usersReducer.users)

    const switchThunkVariable = useSelector( state => state.usersReducer.switchThunk)

    const dispatch = useDispatch();

    const navigateTo = (screenName, params) => {
                                    //dispatch(setUser(params))

                                    //dispatch(fetchAllUsers())

                                    dispatch(setUser(params))

                                    props.navigation.navigate(screenName)
                                    
                                    
                                
                                
                                
                                }

    useEffect(() => {
        if(switchThunkVariable){
            dispatch(fetchAllUsersThunk())

        }else {

        }
       

        
    }, [switchThunkVariable])

    return (
        <View style={{flex: 1, padding: 16 }}>
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold" }}>
                  Welcome to Users App Redux Toolkit
            </Text>

            <View style={{flexDirection: "row"}}>
            <Text style={{color: "#000", fontSize: 16}}> 
                    {"Switch to Thunk: " + switchThunkVariable}
            </Text>

            </View>

            <Switch
                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                    thumbColor={switchThunkVariable ? "#f5dd4b" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={() => dispatch( switchThunk() )}
                    value={switchThunkVariable}
                />

            <FlatList 
                contentContainerStyle={{flex: 1, paddingBottom: 24}}
                data={ users }
                ListEmptyComponent={() =>  
                                        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>

                                            <Text style={{ color: "#000", fontSize: 14, textAlign: "center"}}>No users available</Text>
                                            <Text style={{ color: "#000", fontSize: 14, textAlign: "center"}}>Please start adding users by clicking on ADD USER below.</Text>

                                        </View>}

                renderItem={({item, index}) => {
                    return(

                        <TouchableOpacity onPress = {() => navigateTo("Details", item)}>
                            
                            <View style={{padding: 8, marginVertical: 8,  borderWidth: 1, borderRadius: 4 }}>
                                
                                <Text style={{color: "#000"}}>
                                    {"Name: " +  item.firstName + " " +  item.lastName}
                                </Text>
                                
                                <Text style={{color: "#000"}}>
                                    {"Email: " +  item.email}
                                </Text>
                                {item?.phone ? 
                                                <Text style={{color: "#000"}}>
                                                    {"Phone: " +  item.phone}
                                                </Text> : null
                                }
                                
                                <Text style={{color: "#000"}}>
                                    {"Company: " +  item.company}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />

            <FloatingButtonComponent onPress={() => navigateTo("Edit", {})}/>
        </View>
    )
}

export default HomeScreen; 