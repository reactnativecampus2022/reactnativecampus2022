import * as actions from "../actions/actionTypes"; 

const INITIAL_STATE = {
    users: [], 
    user: {}
}

const userReducer = (state = INITIAL_STATE, action) => {
    switch(action.type){

        case actions.SET_USER:
            return { ...state, user: action.user }

        case actions.ADD_USER:
            var users = [...state.users, action.user ]
            return { ...state, users }

        case actions.UPDATE_USER:
            var users = [...state.users]
            users = users.filter(element => element.id != action.user.id)
            users = [...users, action.user]
            
            return { ...state, users }

        case actions.DELETE_USER:
            var users = [...state.users]
            users = users.filter(element => element.id != action.id)
            
            return { ...state, users  }
    
        default:
            return state
    }
}

export default userReducer