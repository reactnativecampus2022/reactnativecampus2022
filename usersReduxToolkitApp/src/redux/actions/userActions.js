import * as actions from "./actionTypes"

export const setUser = (user) => ({
    type: actions.SET_USER, 
    user
})

export const addUser = (user) => ({
    type: actions.ADD_USER, 
    user 
})

export const updateUser = (user) => ({
    type: actions.UPDATE_USER, 
    user 
})

export const deleteUser = (id) => ({
    type: actions.DELETE_USER, 
    id 
})

