import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'; 
import axios from 'axios';


const INITIAL_STATE = {
    users: [], 
    user: {}, 
    switchThunk: false, 
    loading: false, 
    errors: null
}

export const fetchAllUsersThunk = createAsyncThunk(
    "users/fetchAllUsers", 
    async (payload, thunkAPI) => {

        console.log("@payload: ", payload)
        console.log("@THUNKAPI", thunkAPI)
     
        const response = await axios.get("http://10.0.2.2:3000/users")
        const users = response.data

        return users; 
    }
)

export const addUserThunk = createAsyncThunk(
    "users/addUser", 
    async (payload) => {
     
        const response = await axios.post("http://10.0.2.2:3000/users", payload)
        const user = response.data

        return user; 
    }
)

export const updateUserThunk = createAsyncThunk(
    "users/updateUser", 
    async (payload) => {
     
        const response = await axios.put("http://10.0.2.2:3000/users/" + payload.id, payload)
        const user = response.data

        return user; 
    }
)

export const deleteUserThunk = createAsyncThunk(
    "users/deleteUser", 
    async (payload) => {

        console.log("payload: ", payload)
     
        const response = await axios.delete("http://10.0.2.2:3000/users/"+ payload.id)
        //const users = response.data

        return payload.id; 
    }
)

export const userSlice = createSlice({
    name: "user", 
    initialState: INITIAL_STATE,
    reducers: {

        switchThunk: (state, action) => {
            state.switchThunk = !state.switchThunk
        }, 

        setUser: (state, action) => {
            state.user = action.payload
        }, 

        addUser: (state, action) => {
            state.users.push(action.payload)
        }, 

        updateUser: (state, action) => {
            state.users = state.users.filter(element => element.id != action.payload.id)
            state.users.push(action.payload); 

        }, 

        deleteUser: (state, action) =>{
           console.log("@action: ", action)
            state.users = state.users.filter(element => element.id != action.payload.id)
        }

    },

    extraReducers: {
        //add async reducers here
        [fetchAllUsersThunk.pending]: (state, action) => {
            console.log("fetchAllUsers/pending: ", action)
            state.loading = true;  
        }, 
        [fetchAllUsersThunk.fulfilled]: (state, action) => {
            console.log("fetchAllUsers/fullfilled: ", action)
            state.users = action.payload
            state.loading = false

        },
        [fetchAllUsersThunk.rejected]: (state, action) => {
            console.log("fetchAllUsers/rejected: ", action)
            state.loading = false
        }, 
        [addUserThunk.pending]: (state, action) => {
            console.log("addUserThunk/pending: ", action)
        }, 
        [addUserThunk.fulfilled]: (state, action) => {
            console.log("addUserThunk/fullfilled: ", action)
            //state.users = action.payload
            state.users.push(action.payload)
        },
        [addUserThunk.rejected]: (state, action) => {
            console.log("addUserThunk/rejected: ", action)
        }, 
        [updateUserThunk.pending]: (state, action) => {
            console.log("updateUserThunk/pending: ", action)
        }, 
        [updateUserThunk.fulfilled]: (state, action) => {
            console.log("updateUserThunk/fullfilled: ", action)
            state.users = state.users.filter(element => element.id != action.payload.id)
            state.users.push(action.payload); 
        },
        [updateUserThunk.rejected]: (state, action) => {
            console.log("updateUserThunk/rejected: ", action)
        }, 
        [deleteUserThunk.pending]: (state, action) => {
            console.log("deleteUserThunk/pending: ", action)
        }, 
        [deleteUserThunk.fulfilled]: (state, action) => {
            console.log("deleteUserThunk/fullfilled: ", action)
            //state.users = action.payload
            console.log("state.users: ", state.users); 

            //console.log("action.payload: ", action.payload)
            state.users = state.users.filter(element => element.id != action.payload )
        },
        [deleteUserThunk.rejected]: (state, action) => {
            console.log("deleteUserThunk/rejected: ", action)
        }
    }

})

const {reducer, actions} = userSlice

export const { switchThunk, setUser, addUser, updateUser, deleteUser } = actions
export default reducer