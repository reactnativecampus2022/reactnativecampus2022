import React, { useState, useEffect } from 'react';
import {View, Text, Image, PermissionsAndroid, TouchableOpacity, Linking, Alert } from 'react-native'; 

import ImagePicker from 'react-native-image-crop-picker';

import Share from 'react-native-share';


import files from '../assets/base64/fileBase64'

//Components 
import MyButtonComponent from '../components/MyButtonComponent';

//actions to dispatch
import { deleteUser } from '../redux/actions/userActions'; 

//redux
import { useSelector, useDispatch } from 'react-redux';

const DetailsScreen = (props) =>{

    const user = useSelector(state => state.userReducer.user)
    const dispatch = useDispatch()

    const [image, setImage] = useState("https://reactnative.dev/img/tiny_logo.png")

    
    const requestCameraPermission = async () => {
        try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
            title: "Cool Photo App Camera Permission",
            message:
                "Cool Photo App needs access to your camera " +
                "so you can take awesome pictures.",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
            }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera");
        } else {
            console.log("Camera permission denied");
        }
        } catch (err) {
        console.warn(err);
        }

    }


    const takePhotoFromCamera = () => {
        requestCameraPermission()
        console.log("take photofrom camera")

        ImagePicker.openCamera({
            compressImageMaxWidth: 300,
            compressImageMaxHeight: 300,
            cropping: true,
            compressImageQuality: 0.7
          }).then(image => {
            console.log(image);
            setImage(image.path);
          }).catch(error => console.log(error))

    }

    const chooseFromLibrary = () => {
        console.log("choose from library")

        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            compressImageQuality: 0.7
          }).then(image => {
            console.log(image);
            setImage(image.path);
          }).catch(error => console.log(error))
    }

    const sendMessage = (number) => Linking.openURL(`sms:${number}`);

    const call = (number) => Linking.openURL(`tel:${number}`);

    const email =   (email) =>  Linking.openURL(`mailto:${email}`); 


    const share = () => {
        const options  = {
            title: "MyAwesome App", 
            message: "Share message...",
            //url: files.appLogoShare,
            urls: [files.appLogoShare, files.appLogoShare]
        }

        Share.open(options)
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                err && console.log(err);
            });
    }

    return (
        <View style={{flex: 1, padding: 16 }}>
            
            <Text style={{color: "#000", fontSize: 20, fontWeight: "bold"}}>
                  Welcome to Details Screen
            </Text>
            <View style={{ flex: 1, padding: 16, borderWidth: 1 }} >

                <Text style={{ color: "#000", fontSize: 12, marginBottom: 16 }} >
                    USER INFORMATION
                </Text>

                <Text style={{ color: "#000", fontSize: 12, marginBottom: 16 }} >
                    ID: {"#" + user?.id}
                </Text>
                
                <View style={{alignItems: "center"}}>
                    <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                        Photo
                    </Text>

                    <Image
                        style={{width: 100, height: 100}}
                        //source={require('../assets/img/profile.png')}

                        source={{
                            uri: image,
                          }}
                    />

                    <View>

                        <TouchableOpacity onPress={takePhotoFromCamera} >
                            <View style={{padding: 8, paddingHorizontal: 8, borderWidth: 1, borderRadius: 8}}>
                                    <Text style={{color: "#000", fontSize: 14}}>
                                        Take Photo
                                    </Text>
                                </View>

                        </TouchableOpacity>

                        <TouchableOpacity onPress={chooseFromLibrary}>
                            <View style={{padding: 8, paddingHorizontal: 8, borderWidth: 1, borderRadius: 8}}>
                                <Text style={{color: "#000", fontSize: 14}}>
                                    Choose from library
                                </Text>
                            </View>
                            
                        </TouchableOpacity>
                    </View>


                </View>


                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Name: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.firstName + " " + user?.lastName}
                    </Text>
                </Text>

                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Email: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.email}
                    </Text>
                </Text>

                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Phone: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.phone}
                    </Text>
                </Text>

                <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
                    Company: <Text style={{ color: "#000", fontSize: 16, fontWeight: "normal" }}>
                        {user?.company}
                    </Text>
                </Text>

                <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "stretch" }}>

                    <View>
                        
                    <View style={{   flexDirection: "row", justifyContent: 'space-between', marginVertical: 8}}>
                        <TouchableOpacity onPress={() => sendMessage(user?.phone)} >
                            <View style={{padding: 8, paddingHorizontal: 8, borderWidth: 1, borderRadius: 8}}>
                                    <Text style={{color: "#000", fontSize: 14}}>
                                        Text
                                    </Text>
                                </View>

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => call(user?.phone)}>
                            <View style={{padding: 8, paddingHorizontal: 8, borderWidth: 1, borderRadius: 8}}>
                                <Text style={{color: "#000", fontSize: 14}}>
                                   Call
                                </Text>
                            </View>
                        </TouchableOpacity>
                   
                    
                    
                        <TouchableOpacity onPress={() => email(user?.email)} >
                            <View style={{padding: 8, paddingHorizontal: 8, borderWidth: 1, borderRadius: 8}}>
                                    <Text style={{color: "#000", fontSize: 14}}>
                                        Email
                                    </Text>
                                </View>

                        </TouchableOpacity>

                        <TouchableOpacity onPress={share} >
                            <View style={{padding: 8, paddingHorizontal: 8, borderWidth: 1, borderRadius: 8}}>
                                    <Text style={{color: "#000", fontSize: 14}}>
                                        Share
                                    </Text>
                                </View>

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => props.navigation.navigate("Map")}  >
                            <View style={{padding: 8, paddingHorizontal: 8, borderWidth: 1, borderRadius: 8}}>
                                    <Text style={{color: "#000", fontSize: 14}}>
                                        Show Map
                                    </Text>
                                </View>

                        </TouchableOpacity>

                        </View>
                       
                    </View>


                    <Text style={{ color: "#000", fontSize: 16 }}>
                        Member since {user?.createdAt}
                    </Text>

                </View>

            </View>

            <View >
                <View style={{flexDirection: "row", marginVertical: 8, justifyContent: "space-around"}}>
                    {/* <MyButtonComponent onPress= {() => console.log("Call Me")}
                                backgroundColor= "#00F"
                                title="Call" />
                    
                    <MyButtonComponent onPress= {() => console.log("Send Email")}
                                backgroundColor= "#F0F"
                                title="Email" /> */}
                
                </View>
                <View style={{flexDirection: "row",  justifyContent: "space-around"}}>
                        <MyButtonComponent onPress= {() => props.navigation.navigate("Edit")}
                                        backgroundColor= "#00F"
                                        title="Edit" />

                        <MyButtonComponent onPress= {() =>{ dispatch(deleteUser(user?.id))
                                                            props.navigation.navigate("Home") 
                                                        }}
                                            backgroundColor= "#F0F"
                                            title="Delete" />
       
                </View>            
            </View>
        </View>
    )
}

export default DetailsScreen 