import React from 'react'; 
import {View, Text, Image } from "react-native"; 

import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps'; 

const MapScreen = (props) => {
    const {route, navigation} = props
    
    return(
        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
            {/* <Text  style={{textAlign: "center", color: "#000", fontSize: 14}}>Map screen</Text> */}
            <MapView
                style={{flex: 1,position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,}}
                provider={PROVIDER_GOOGLE}
                initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}>

                    <Marker
                        //key={0}
                        coordinate={{
                            latitude: 37.78825,
                                longitude: -122.4324,}}
                        title='Test Title'
                        description='TEST DESCRIPTION'
                        >
                            <Callout tooltip
                                        onPress={() => console.log("CALLOUT PRESSES")}
                            >
                                <View style={{ backgroundColor: '#fcd000', padding: 16}}>
                                    
                                    
                                    <Text style={{fontSize: 14, color: "#000", fontWeight: "bold"}}>
                                        Welcome to my awesome company    
                                    </Text>

                                    <Text style={{fontSize: 12, color: "#000"}}>
                                        Press the marker for more info    
                                    </Text>       
                                </View> 
                            </Callout>
                        </Marker>
                </MapView>
        </View>
    )
}

export default MapScreen; 