import {createStore} from "redux"; 

import rootReducer from "./reducers/rootReducer";

//my global store
const store = createStore(rootReducer)

export default store