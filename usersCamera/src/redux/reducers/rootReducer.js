import { combineReducers } from "redux";

//Reducers
import userReducer from "../reducers/userReducer"; 

const rootReducer = combineReducers({
    userReducer: userReducer
})

export default rootReducer; 